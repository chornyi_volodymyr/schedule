package com.softserveacademy.jsonService;

import com.softserveacademy.model.Event;

import java.io.File;
import java.util.Set;

public class ThreadExample implements Runnable{

    File testThreadFile = new File("E:\\SoftServe\\Schedule\\src\\main\\java\\com\\softserveacademy\\jsonService\\testThread.json");
    Set<Event> events;

    public ThreadExample(Set<Event> events) {
        this.events = events;
    }

    @Override
    public void run() {
        GsonService gsonService = new GsonService();
        gsonService.writeEventListToFile(testThreadFile, events);

    }
}
