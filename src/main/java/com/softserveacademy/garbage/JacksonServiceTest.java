//package com.softserveacademy.json;
//
//import com.softserveacademy.exception.SomethingWentWrongException;
//import com.softserveacademy.exception.UneducatedTeacherException;
//import com.softserveacademy.model.*;
//import org.junit.Test;
//
//import java.io.File;
//import java.io.IOException;
//import java.time.DayOfWeek;
//import java.util.HashSet;
//import java.util.Set;
//
//import static org.junit.Assert.*;
//
//public class JacksonServiceTest {
//
//    Schedule schedule = new Schedule();
//    Teacher teacher = new Teacher(1, 45,"Stepan", "Ivanov");
//    Group group = new Group(102);
//    Subject subject = new Subject("Phi");
//    Room room = new Room(2);
//    Event event = new Event(DayOfWeek.MONDAY, Event.NumberEvent.SECOND, teacher, group, subject, room);
//    Event event2 = new Event(DayOfWeek.FRIDAY, Event.NumberEvent.SECOND, teacher, group, subject, room);
//    Event event3 = new Event(DayOfWeek.MONDAY, Event.NumberEvent.FIFTH, teacher, group, subject, room);
//
//    JacksonService jacksonService = new JacksonService();
//
//    public JacksonServiceTest() throws UneducatedTeacherException {
//    }
//
//    @Test
//    public void writeEventListToFile() throws IOException, SomethingWentWrongException, UneducatedTeacherException {
//        File file = new File("E:\\SoftServe\\Schedule\\src\\test\\java\\com\\softserveacademy\\json\\emptyTestFile.json");
//        if(file.exists())
//            file.delete();
//        file.createNewFile();
//        assertEquals(0, file.length());
//        teacher.addSkill(subject);
//        schedule.addEvent(event);
//        schedule.addEvent(event2);
//        jacksonService.writeEventListToFile(file, schedule.getEventList());
//        assertFalse(file.length() == 0);
//    }
//
//    @Test
//    public void readEventsFromFile() {
//        File file = new File("E:\\SoftServe\\Schedule\\src\\test\\java\\com\\softserveacademy\\json\\testFile.json");
//        Set<Event> set = new HashSet<>();
//        assertEquals(0, set.size());
//        assertFalse(file.length() == 0);
//        set = jacksonService.readEventsFromFile(file);
//        assertTrue (set.size() > 0);
//    }
//
//    @Test
//    public void addEventsFromFileToEventList() {
//        File file = new File("E:\\SoftServe\\Schedule\\src\\test\\java\\com\\softserveacademy\\json\\testFile.json");
//        int startSize = schedule.getEventList().size();
//        jacksonService.addEventsFromFileToEventList(file, schedule);
//        int endSize = schedule.getEventList().size();
//        assertTrue(endSize > startSize);
//    }
//}