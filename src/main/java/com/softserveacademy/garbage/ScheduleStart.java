package com.softserveacademy.garbage;

import com.softserveacademy.dao.JdbcService;
import com.softserveacademy.jsonService.ThreadExample;
import com.softserveacademy.model.*;
import com.softserveacademy.service.EventCreator;
import com.softserveacademy.service.Schedule;

import java.io.*;
import java.time.DayOfWeek;

public class ScheduleStart {

    public static void main(String[] args) throws IllegalArgumentException {
        Schedule schedule = new Schedule();
        EventCreator eventCreator = new EventCreator();

        Teacher ivanPetrov = new Teacher("Ivan", "Petrov");
        Teacher ivanStepanov = new Teacher("Ivan", "Stepanov");
        Group g101 = new Group(101);
        Group g102 = new Group(102);
        Student borya = new Student("Borya", "Gunya");
        Student semen = new Student( "Semen", "Somov");
        g101.setStudent(borya);
        g102.setStudent(semen);
        Room r1 = new Room(1, "3");
        Room r2 = new Room(2, "5");

        Subject phi = new Subject("Phi");


        Event event = eventCreator.setDayOfWeek(DayOfWeek.MONDAY)
                .setNumberEvent(NumberEvent.SECOND)
                .setTeacher(ivanPetrov)
                .setGroup(g101)
                .setSubject(phi)
                .setRoom(r1)
                .setSubjectToTeacher(ivanPetrov, phi)
                .create();

        schedule.addEvent(event);

//
//        File inputFile = new File("E:\\SoftServe\\Schedule\\src\\main\\java\\com\\softserveacademy\\json\\input.json");
//        File outputFile = new File("E:\\SoftServe\\Schedule\\src\\main\\java\\com\\softserveacademy\\json\\output.json");
//
////        JacksonService jacksonService = new JacksonService();
//
////        jacksonService.writeEventListToFile(outputFile, schedule.getEventList());
////        jacksonService.writeEventListToFile(outputFile, schedule.getDailyEvents(borya, DayOfWeek.MONDAY));
////        Set set = jacksonService.readEventsFromFile(outputFile);
////        System.out.println(set.size());
//
//        GsonService gsonService = new GsonService();
////        gsonService.writeEventListToFile(outputFile, schedule.getEventList());
////        System.out.println(gsonService.str);
////        Set<Event> set = gsonService.readEventsFromFile(outputFile);
//
//
////        schedule.addEvent(set);
////        for (Event e:set
////             ) {
////            System.out.println(e);
////
////        }
//
//    ObjectGenerator objectGenerator = new ObjectGenerator();
//    ThreadExample threadExample = new ThreadExample();
//
//
//        long startTime = System.currentTimeMillis();
//
//        threadExample.run();
////        for (DayOfWeek dayOfWeek: DayOfWeek.values()) {
////            for (Event.NumberEvent numberEvent: Event.NumberEvent.values()) {
////                for (int i = 0; i < 10; i++){
////                    Event event = objectGenerator.generateEvent(dayOfWeek, numberEvent);
////                    schedule.addEvent(event);
////                }
////            }
////        }
//
////        long startTime = System.currentTimeMillis();
//        gsonService.writeEventListToFile(outputFile, schedule.getEventList());
//        long endTime = System.currentTimeMillis();
//        int i = gsonService.readEventsFromFile(outputFile).size();
//        System.out.println(endTime - startTime);
//        System.out.println(i);
//    }
//
//        JdbcService jdbcService = new JdbcService();
//        jdbcService.createTable();

    ThreadExample threadExample = new ThreadExample(schedule.getEvents());
    threadExample.r

    }
}
