//package com.softserveacademy.json;
//
//import com.softserveacademy.exception.SomethingWentWrongException;
//import com.softserveacademy.exception.UneducatedTeacherException;
//import com.softserveacademy.model.Event;
//import com.softserveacademy.service.Schedule;
//
//import org.codehaus.jackson.map.ObjectMapper;
//import org.codehaus.jackson.map.ObjectWriter;
//
//import java.io.File;
//import java.io.IOException;
//import java.util.HashSet;
//import java.util.Set;
//
//public class JacksonService {
//
//    public ObjectMapper mapper = new ObjectMapper();
//    private ObjectWriter writer = mapper.writer();
//
//    public void writeEventListToFile (File file, Set<Event> eventList){
//        try {
//            if (!file.exists())
//                file.createNewFile();
//            writer.writeValue(file, eventList);
//        }catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
//
//    public Set<Event> readEventsFromFile (File file){
//        Set<Event> set = new HashSet<>();
//        try {
//            if (file.exists())
//                set = mapper.readValue(file, HashSet.class);
//        }catch (IOException e) {
//            e.printStackTrace();
//        }
//        return set;
//    }
//
//    public void addEventsFromFileToEventList (File file, Schedule schedule){
//        try {
//            if (file.exists())
//                schedule.addEvent(mapper.readValue(file, HashSet.class));
//        }catch (IOException | SomethingWentWrongException | UneducatedTeacherException e) {
//            e.printStackTrace();
//        }
//    }
//}