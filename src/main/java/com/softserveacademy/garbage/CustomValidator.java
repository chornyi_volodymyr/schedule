package com.softserveacademy.garbage;

public class CustomValidator {

    public int validateAge(int age){
        if (age < 16 && age > 100)
            throw new IllegalArgumentException("incorrect age");
        return age;
    }

    public int validateId(int id){
        if (id < 0 && id > 100000)
            throw new IllegalArgumentException("incorrect id");
        return id;
    }

    public String validateName(String name){
        if(name.isEmpty())
            throw new IllegalArgumentException("the field cannot be empty");
        return name;
    }

    public int validateNumber(int number){
        if (number < 1 && number > 1000)
            throw new IllegalArgumentException("incorrect age");
        return number;
    }
}
