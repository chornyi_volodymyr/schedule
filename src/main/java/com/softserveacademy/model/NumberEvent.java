package com.softserveacademy.model;

public enum NumberEvent {
    FIRST, SECOND, THIRD, FOURTH, FIFTH, SIXTH, SEVENTH, EIGHTH
}
